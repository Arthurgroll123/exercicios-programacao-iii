# Exercícios Programação III

Exercícios da disciplina de Programação III do IFRS - Campus Canoas, ministrada pelo professor Leonardo Carvalho, no ano de 2022.

## Como rodar/testar os exercícios

Para testar os exercícios é possível apenas baixar a pasta com o nome da aula desejada e rodar utilizando um web server de sua preferência, bastando apenas colar a pasta da aula no diretório que seu web server vai utilizar para servir arquivos, como por exemplo a pasta htdocs no caso do XAMPP.

Também é possível rodar utilizando os scripts presentes na pasta raiz do repositório (não esqueça de deixar o script do exercício e o exercício sobre um mesmo diretório pai, como C:/teste/aula01 e C:/teste/aula01.sh), que utilizam sempre o padrão run_&lt;aula desejada&gt;_&lt;plataforma desejada&gt;.&lt;sh ou bat&gt; (ex: o script run_aula01_unix.sh irá rodar os exercícios da aula 1 na grande maioria dos sistemas baseados em Unix, e run_aula02_win.bat irá rodar os exercícios da aula 2 no Windows). Contudo, é necessário possuir o Docker e o Docker Compose instalados.

Caso você não possua nem o Docker nem o Docker Compose e estiver utilizando Ubuntu, basta rodar o script install_dependencies.sh na pasta raiz do repositório e o Docker e Docker Compose serão instalados na sua máquina, e bastará rodar o script da aula desejada.

Já se você for um usuário Windows, você deve <a href="https://desktop.docker.com/win/main/amd64/Docker%20Desktop%20Installer.exe">baixar o Docker Desktop</a> e instalá-lo. Após isso, basta rodar o script da aula que você deseja ver.