<?php

    function getNumero($escolha)
    {
        if ($escolha == "pedra") return 0;
        if ($escolha == "papel") return 1;
        if ($escolha == "tesoura") return 2;
        if ($escolha == "lagarto") return 3;
        if ($escolha == "spok") return 4;
    }

    function getString($escolha)
    {
        if ($escolha == 0) return "Pedra";
        if ($escolha == 1) return "Papel";
        if ($escolha == 2) return "Tesoura";
        if ($escolha == 3) return "Lagarto";
        if ($escolha == 4) return "Spok";
    }

    $escolha = getNumero($_POST["escolha"]);

    $maquina = rand(0, 4);

    echo "<div class=\"text-center\"><p>Escolha da máquina: " . getString($maquina) . "</p><p>";

    // if-else utilizado talvez não da maneira mais correta, mas isso foi feito para melhorar a visibilidade do código

    if ($maquina == $escolha)
    {
        echo "Empate!";
    }
    else if ($escolha == 0) // Pedra
    {
        if ($maquina == 1 || $maquina == 4) // Papel ou Spok
        {
            echo "Você perdeu!";
        }
        else
        {
            echo "Você ganhou!";
        }
    }
    else if ($escolha == 1) // Papel
    {
        if ($maquina == 2 || $maquina == 3) // Tesoura ou Lagarto
        {
            echo "Você perdeu!";
        }
        else
        {
            echo "Você ganhou!";
        }
    }
    else if ($escolha == 2) // Tesoura
    {
        if ($maquina == 4 || $maquina == 0) // Spok ou Pedra
        {
            echo "Você perdeu!";
        }
        else
        {
            echo "Você ganhou!";
        }
    }
    else if ($escolha == 3) // Lagarto
    {
        if ($maquina == 0 || $maquina == 2) // Pedra ou Tesoura
        {
            echo "Você perdeu!";
        }
        else
        {
            echo "Você ganhou!";
        }
    }
    else if ($escolha == 4) // Spok
    {
        if ($maquina == 1 || $maquina == 3) // Papel ou Lagarto
        {
            echo "Você perdeu!";
        }
        else
        {
            echo "Você ganhou!";
        }
    }

    echo "</p></div>";
?>