<?php

    $numero = $_POST["numero"];

    echo "<div class=\"text-center\">";

    if ($numero == 5)
    {
        echo "Número igual a 5";
    }
    else if ($numero == 200)
    {
        echo "Número igual a 200";
    }
    else if ($numero == 400)
    {
        echo "Número igual a 400";
    }
    else if ($numero > 500 && $numero <= 1000)
    {
        echo "Número maior que 500 e menor ou igual a 1000";
    }
    else
    {
        echo "Não satisfaz nenhuma das opções!";
    }

    echo "</div>";

?>