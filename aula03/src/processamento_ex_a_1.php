<?php
    $nome = $_POST["nome"];
    $valorUnitario = $_POST["precoUnitario"];
    $quantidadeVendida = $_POST["quantidadeVendida"];

    $valorTotal = $valorUnitario * $quantidadeVendida;

    echo "<div class=\"text-center\">Venda do lote do produto \"" . $nome . "\" foi estimada em R$ " . number_format($valorTotal, 2, ",") . "!<br/>" . ($valorTotal > 1000.0 ? "Vendedor receberá 10% do valor!" : "") . "</div>";
?>