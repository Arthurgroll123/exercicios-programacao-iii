<table class="col-6 table table-striped table-bordered">
    <thead>
        <tr>
            <th>Classificação</th>
            <th>Informação</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Nome</td>
            <td><?php echo $_POST["nome"]; ?></td>
        </tr>
        <tr>
            <td>E-Mail</td>
            <td><?php echo $_POST["email"]; ?></td>
        </tr>
        <tr>
            <td>Data de Nascimento</td>
            <td><?php echo $_POST["dataNascimento"]; ?></td>
        </tr>
        <tr>
            <td>Sexo</td>
            <td><?php echo $_POST["sexo"]; ?></td>
        </tr>
        <tr>
            <td>Estado</td>
            <td><?php echo $_POST["estado"]; ?></td>
        </tr>
        <tr>
            <td>Cidade</td>
            <td><?php echo $_POST["cidade"]; ?></td>
        </tr>
        <tr>
            <td>Bairro</td>
            <td><?php echo $_POST["bairro"]; ?></td>
        </tr>
        <tr>
            <td>Endereço</td>
            <td><?php echo $_POST["endereco"]; ?></td>
        </tr>
        <tr>
            <td>CEP</td>
            <td><?php echo $_POST["cep"]; ?></td>
        </tr>
        <tr>
            <td>Email Promocional</td>
            <td><?php echo ($_POST["receberEmails"] == "true" ? "Quer receber" : "Não quer receber"); ?></td>
        </tr>
    </tbody>
</table>