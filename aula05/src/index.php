<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="./shared/styles.css"/>
    <title>Menu - Aula 05</title>
</head>
<body>
    <h1>Exercícios</h1>
    <div>
        <div>
            <a href="./exercicio1/index.php">Exercício 1</a>
            <div class="w-100"></div>
            <a href="./exercicio2/index.html">Exercício 2</a>
            <div class="w-100"></div>
            <a href="./exercicio3/index.php">Exercício 3</a>
            <div class="w-100"></div>
            <a href="./exercicio4/index.php">Exercício 4</a>
            <div class="w-100"></div>
            <a href="./exercicio5/index.php">Exercício 5</a>
            <div class="w-100"></div>
            <a href="./exercicio6/index.php">Exercício 6</a>
            <div class="w-100"></div>
        </div>
    </div>
</body>
</html>