<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../shared/styles.css">
    <title>Aula 05 - Exercício 3</title>
</head>
<body>
    <h1>Exercício 3</h1>
    <div>
        <?php
            $matrizHotweels = [
                "Vw" => [
                    "Vw hotweels versão 1",
                    "Vw hotweels versão 2",
                    "Vw hotweels versão 3",
                    "Vw hotweels versão 4"
                ],
                "GM" => [
                    "GM hotweels versão 1",
                    "GM hotweels versão 2",
                    "GM hotweels versão 3",
                    "GM hotweels versão 4"
                ],
                "Fiat" => [
                    "Fiat hotweels versão 1",
                    "Fiat hotweels versão 2",
                    "Fiat hotweels versão 3",
                    "Fiat hotweels versão 4"
                ],
                "Ford" => [
                    "Ford hotweels versão 1",
                    "Ford hotweels versão 2",
                    "Ford hotweels versão 3",
                    "Ford hotweels versão 4"
                ]
            ];

            echo "<b>Carro 3 da Vw:</b> " . $matrizHotweels["Vw"][2] .
            "<br/><b>Carro 1 da Fiat:</b> " . $matrizHotweels["Fiat"][0] .
            "<br/><b>Carro 2 da Ford:</b> " . $matrizHotweels["Ford"][1];
        ?>
    </div>
</body>
</html>