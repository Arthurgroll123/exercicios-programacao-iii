<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../shared/styles.css">
    <title>Aula 05 - Exercício 4</title>
</head>
<body>
    <h1>Exercício 4</h1>
    <div>
        <?php
            $numPessoas = 15;
            /**********************************************************************************************
             * OBSERVAÇÃO IMPORTANTE: NÃO FOI DITO PARA TER O CAMPO SEXO, PORÉM SÓ É POSSÍVEL SABER       *
             * SE A PESSOA É DO SEXO MASCULINO OU FEMININO SE RECEBERMOS ESSE VALOR DO USUÁRIO; PORTANTO, *
             * FOI ADICIONADO UM INPUT RADIO COM O SEXO                                                   *
             **********************************************************************************************/

            function printarOpcoes($numOpcao)
            {
                echo '<label>Nome </label>';
                echo '<input class="name-field field-form" type="text" name="nomePessoa[]"/>';
                echo '<br/>';
                echo '<label>Idade </label>';
                echo '<input class="age-field field-form" type="number" name="idadePessoa[]"/>';
                echo '<br/>';
                echo '<label>Sexo</label><br/>';
                echo '<label for="masc-' . $numOpcao . '">Masculino</label>';
                echo '<input id="masc-' . $numOpcao . '" class="gender-field" type="radio" name="sexoPessoa' . $numOpcao . '" value="Masculino"/>';
                echo '<br/>';
                echo '<label for="fem-' . $numOpcao . '">Feminino</label>';
                echo '<input id="fem-' . $numOpcao . '" class="gender-field" type="radio" name="sexoPessoa' . $numOpcao . '" value="Feminino"/>';
            }

            for ($i = 0; $i < $numPessoas; $i++)
            {
                echo "<h4>Pessoa ". ($i + 1) . "</h4>";
                printarOpcoes($i);
                echo '<br/>';
            }
        ?>

        <input type="submit" onclick="enviarForm()" value="Enviar"/>

        <h1>Response:</h1>
        <div id="response"></div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script>
        function checkForm()
        {
            if ($("input[name^='sexoPessoa']:checked").length < <?php echo $numPessoas; ?>)
            {
                window.alert("Preencha todos os campos do formulário!");
                return false;
            }

            Array.from($("input.field-form")).forEach(elem => {
                if (elem.value.length == 0)
                {
                    window.alert("Preencha todos os campos do formulário!");
                    return false;
                }
            });

            return true;
        }

        function enviarForm()
        {
            if (!checkForm())
                return;

            $("#response").html("<h1>Carregando...</h1>");

            let usuarios = [];
            let nomes = [];
            let idades = [];

            Array.from($('input[name="nomePessoa[]"]')).forEach(input => {
                nomes.push($(input).val());
            });

            Array.from($('input[name="idadePessoa[]"]')).forEach(input => {
                idades.push($(input).val());
            });

            for (let i = 0; i < <?php echo $numPessoas; ?>; i++)
            {
                usuarios.push(JSON.stringify({
                    nome: nomes[i],
                    idade: idades[i],
                    sexo: $("input[name='sexoPessoa" + i + "']:checked").val()
                }));
            }

            $.ajax({
                url: './processamento.php',
                type: 'POST',
                data: {
                    "usuarios[]": usuarios
                },
                success: (res) => {
                    $("#response").html(res);
                },
                error: (res) => {
                    $("#response").html(res);
                }
            });
        }
    </script>
</body>
</html>