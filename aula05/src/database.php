<?php
    class DatabaseManager
    {
        public PDO $connection;

        public function __construct()
        {
            $host = 'db';
            $db   = 'aula05';
            $user = 'root';
            $pass = 'abcd1234';
            $charset = 'utf8mb4';

            $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
            $options = [
                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_EMULATE_PREPARES   => false,
            ];

            try {
                $this->connection = new PDO($dsn, $user, $pass, $options);
            } catch (\PDOException $e) {
                throw new \PDOException($e->getMessage(), (int)$e->getCode());
            }
        }

        public function getPDO()
        {
            return $this->connection;
        }
    }

    $GLOBALS['dbSingleton'] = new DatabaseManager;
?>