<?php
    $info = json_decode($_POST["info"]);

    /* Devido a eu ter feito assim (sobre o porquê de eu ter feito assim não
        tem muita lógica, simplesmente acabei fazendo assim, até porque as "contas" são mais fáceis,
        e esqueci que talvez o uso de uma matriz mesmo que era o que era necessário) Como agora
        são 5:35 da manhã, eu vou deixar aqui uma matriz de exemplo, porque tô cansado demais
        pra arrumar/fazer de outro jeito:

        $matriz = [
            [1, 2, 3, 4],
            [5, 6, 7, 8],
            [9, 10, 11, 12]
        ];

        echo $matriz[0][0]; // retorna 1
        echo $matriz[1][3]; // retorna 8
        echo $matriz[2][1]; // retorna 10
    */

    function printMatrix($descMatriz, $matrix, $rows, $cols, $transpor = false)
    {
        echo "<p><b>$descMatriz:</b></p>";
        echo "<table class='table-mod'><tbody>";

        if (!$transpor)
        {
            for ($i = 0; $i < $rows; $i++)
            {
                echo "<tr>";
                for ($j = 0; $j < $cols; $j++)
                {
                    echo "<td>" . $matrix[$cols * $i + $j] . "</td>";
                }
                echo "</tr>";
            }
        }
        else
        {
            for ($i = 0; $i < $rows; $i++)
            {
                echo "<tr>";
                for ($j = 0; $j < $cols; $j++)
                {
                    echo "<td>" . $matrix[$i + $j * $rows] . "</td>";
                }
                echo "</tr>";
            }
        }

        echo "</tbody></table>";
    }

    printMatrix("Matriz original", $info->matrix, $info->rows, $info->cols);

    printMatrix("Matriz transposta", $info->matrix, $info->cols, $info->rows, true);
?>