<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../shared/styles.css">
    <title>Aula 05 - Exercício 6</title>
</head>
<body>
    <h1>Exercício 6</h1>
    <div>
        <?php
            $rows = 3;
            $cols = 4;

            function printarOpcoes($numOpcao)
            {
                echo '<label>Coluna ' . $numOpcao . ' </label>';
                echo '<input class="matrix-field field-form" type="text" name="matrix[]"/>';
                echo '<span style="margin-left: 10px;"></span>';
            }

            for ($i = 0; $i < $rows; $i++)
            {
                echo "<h4>Linha ". ($i + 1) . "</h4>";
                for ($j = 0; $j < $cols; $j++)
                {
                    printarOpcoes($j + 1);
                }
                echo '<br/>';
            }
        ?>

        <input style="margin-top: 10px;" type="submit" onclick="enviarForm()" value="Enviar"/>

        <h1>Response:</h1>
        <div id="response"></div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script>
        function checkForm()
        {
            Array.from($("input.field-form")).forEach(elem => {
                if (elem.value.length == 0)
                {
                    window.alert("Preencha todos os campos do formulário!");
                    return false;
                }
            });

            return true;
        }

        function enviarForm()
        {
            if (!checkForm())
                return;

            $("#response").html("<h1>Carregando...</h1>");

            let info = {
                rows: <?php echo $rows; ?>,
                cols: <?php echo $cols; ?>,
                matrix: []
            };

            Array.from($('input[name="matrix[]"]')).forEach(input => {
                info.matrix.push($(input).val());
            });

            $.ajax({
                url: './processamento.php',
                type: 'POST',
                data: {
                    "info": JSON.stringify(info)
                },
                success: (res) => {
                    $("#response").html(res);
                },
                error: (res) => {
                    $("#response").html(res);
                }
            });
        }
    </script>
</body>
</html>