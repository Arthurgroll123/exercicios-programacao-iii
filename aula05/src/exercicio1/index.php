<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../shared/styles.css"/>
    <title>Aula 05 - Exercício 1</title>
</head>
<body>
    <h1>Exercício 1</h1>
    <div>
        <?php
            $vetor = [
                "ING" => "Inglaterra",
                "EUA" => "Estados Unidos",
                "BRA" => "Brasil"
            ];

            foreach ($vetor as $sigla => $pais)
            {
                echo "<p>\"$sigla\" = \"$pais\"</p>"; 
            }
        ?>
    </div>
</body>
</html>