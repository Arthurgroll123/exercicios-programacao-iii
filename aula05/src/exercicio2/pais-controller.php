<?php
    require_once "./pais-model.php";
    
    $vetorAcoes = [
        "getPaises",
        "insertPais"
    ];

    if (!isset($_POST["acao"]) || array_key_exists($_POST["acao"], $vetorAcoes))
    {
        die();
    }

    if ($_POST["acao"] == "insertPais")
    {
        if (!isset($_POST["sigla"]) || !isset($_POST["pais"]))
        {
            die();
        }

        insertPais($_POST["sigla"], $_POST["pais"]);

        echo "OK";
    }
    else if ($_POST["acao"] == "getPaises")
    {
        echo getPaises();
    }

    function getPaises()
    {
        $res = "";

        foreach (Pais::getPaises() as $pais)
        {
            $res .= $pais->getView(true);
        }

        return $res;
    }

    function insertPais($sigla, $nome)
    {
        (new Pais($sigla, $nome))->insertIntoDatabase();
    }
?>