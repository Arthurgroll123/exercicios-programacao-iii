<?php
    require_once "../database.php";

    class Pais
    {
        public string $sigla;
        public string $pais;

        public function __construct($sigla, $pais)
        {
            $this->sigla = $sigla;
            $this->pais = $pais;
        }

        public function insertIntoDatabase()
        {
            $stmt = $GLOBALS['dbSingleton']->getPDO()->prepare("SELECT COUNT(*) FROM pais WHERE sigla=:sigla AND pais=:pais");
            $stmt->execute(["sigla" => $this->sigla, "pais" => $this->pais]);
            $value = $stmt->fetchColumn();

            if ($value > 0)
                return;

            $stmt = $GLOBALS['dbSingleton']->getPDO()->prepare("INSERT INTO pais(sigla, pais) VALUES (:sigla, :pais)");
            $stmt->execute(["sigla" => $this->sigla, "pais" => $this->pais]);
        }

        public static function getPaises()
        {
            $paisesVetor = [];
            $stmt = $GLOBALS['dbSingleton']->getPDO()->prepare("SELECT * FROM pais");
            $stmt->execute();

            $paises = $stmt->fetchAll();

            foreach ($paises as $pais)
            {
                array_push($paisesVetor, new Pais($pais["sigla"], $pais["pais"]));
            }

            return $paisesVetor;
        }

        public function getView($returnString = false, $isTable = true)
        {
            $str = "";

            if ($isTable)
            {
                $str = "<tr><td>$this->sigla</td><td>$this->pais</td></tr>";
            }
            else
            {
                $str = "<p>$this->sigla = $this->pais</p>";
            }

            if ($returnString)
                return $str;
            
            echo $str;
        }
    }
?>