<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../shared/styles.css">
    <title>Aula 05 - Exercício 5</title>
</head>
<body>
    <h1>Exercício 5</h1>
    <div>
        <?php
            $numPessoas = 10;

            function printarOpcoes($numOpcao)
            {
                echo '<label>Nome </label>';
                echo '<input class="name-field field-form" type="text" name="nomePessoa[]"/>';
                echo '<br/>';
                echo '<label>Nota </label>';
                echo '<input class="nota1-field field-form" type="number" name="notaPessoa[]"/>';
                echo '<br/>';
            }

            for ($i = 0; $i < $numPessoas; $i++)
            {
                echo "<h4>Participante ". ($i + 1) . "</h4>";
                printarOpcoes($i);
                echo '<br/>';
            }
        ?>

        <input type="submit" onclick="enviarForm()" value="Enviar"/>

        <h1>Response:</h1>
        <div id="response"></div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script>
        function checkForm()
        {
            Array.from($("input.field-form")).forEach(elem => {
                if (elem.value.length == 0)
                {
                    window.alert("Preencha todos os campos do formulário!");
                    return false;
                }
            });

            return true;
        }

        function enviarForm()
        {
            if (!checkForm())
                return;

            $("#response").html("<h1>Carregando...</h1>");

            let participantes = [];
            let nomes = [];
            let nota = [];

            Array.from($('input[name="nomePessoa[]"]')).forEach(input => {
                nomes.push($(input).val());
            });

            Array.from($('input[name="notaPessoa[]"]')).forEach(input => {
                nota.push($(input).val());
            });

            for (let i = 0; i < <?php echo $numPessoas; ?>; i++)
            {
                participantes.push(JSON.stringify({
                    nome: nomes[i],
                    nota: nota[i],
                }));
            }

            $.ajax({
                url: './processamento.php',
                type: 'POST',
                data: {
                    "participantes[]": participantes
                },
                success: (res) => {
                    $("#response").html(res);
                },
                error: (res) => {
                    $("#response").html(res);
                }
            });
        }
    </script>
</body>
</html>