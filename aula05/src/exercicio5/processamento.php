<?php
    $users = $_POST["participantes"];

    $melhores = [
        [
            "nome" => "",
            "nota" => ""
        ],
        [
            "nome" => "",
            "nota" => ""
        ],
        [
            "nome" => "",
            "nota" => ""
        ]
    ];

    foreach ($users as $user)
    {
        $userParsed = json_decode($user);

        if ($userParsed->nota > $melhores[0]["nota"])
        {
            $melhores[2] = $melhores[1];
            $melhores[1] = $melhores[0];
            $melhores[0] = [
                "nome" => $userParsed->nome,
                "nota" => $userParsed->nota
            ];
        }
        else if ($userParsed->nota > $melhores[1]["nota"])
        {
            $melhores[2] = $melhores[1];
            $melhores[1] = [
                "nome" => $userParsed->nome,
                "nota" => $userParsed->nota
            ];
        }
        else if ($userParsed->nota > $melhores[2]["nota"])
        {
            $melhores[2] = [
                "nome" => $userParsed->nome,
                "nota" => $userParsed->nota
            ];
        }
    }

    echo "<p><b>Melhores:</b></p>";
    echo "<table><thead><tr><th>Nome</th><th>Nota</th></tr></thead><tbody>";

    for ($i = 0; $i < 3; $i++)
    {
        echo "<tr><td>" . $melhores[$i]["nome"] . "</td><td>" . $melhores[$i]["nota"] . "</td>";
    }

    echo "</tbody></table>";
?>