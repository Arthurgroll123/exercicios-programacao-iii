<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        .mb {
            margin-bottom: 10px;
        }
        .container {
            margin-left: auto;
            margin-right: auto;
            width: fit-content;
            text-align: center;
        }
    </style>
    <title>Questão 1 - ASP 01</title>
</head>
<body>
    <div class="mb container">
        <label for="altura">Altura</label>
        <input class="mb" type="number" name="altura" id="altura" step="0.01">
        <br/>
        <label for="peso">Peso</label>
        <input class="mb" type="number" name="peso" id="peso" step="0.01">
        <br/>
        <label for="genero">Gênero</label>
        <select class="mb" name="genero" id="genero">
            <option value="Masculino">Masculino</option>
            <option value="Feminino">Feminino</option>
        </select>
        <br/>
        <button class="btn btn-primary" onclick="enviarForm()">Enviar</button>
    </div>

    <div id="response" class="container"></div>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script>
        function enviarForm()
        {
            $("#response").html("<p>Carregando...</p>");
            $.ajax({
                url: "./processamento.php",
                type: "POST",
                data: {
                    altura: $("#altura").val(),
                    peso: $("#peso").val(),
                    genero: $("#genero").find(":selected").val()
                },
                success: (res) => {
                    $("#response").html(res);
                },
                error: (res) => {
                    $("#response").html(res);
                }
            });
        }
    </script>
</body>
</html>