<?php
    if (!isset($_POST["peso"]) || !isset($_POST["altura"]) || !isset($_POST["genero"]))
    {
        echo "PARÂMETROS ENVIADOS AO POST FALTANDO!";
        die();
    }

    if ($_POST["altura"] == 0)
    {
        echo "Altura igual a zero, reveja os valores inseridos!";
        die();
    }

    $imc = (float)$_POST["peso"] / ((float)$_POST["altura"] * (float)$_POST["altura"]);

    $pesoIdeal = $_POST["genero"] == "Masculino" ? (72.7 * (float)$_POST["altura"] - 58.0) : (62.1 * (float)$_POST["altura"] - 44.7);

    echo "<b>OBS: Os valores indicados são apenas indicadores, e desconsideram a distribuição de peso e massa muscular!</b><br/>";
    echo "<p>O IMC é: " . number_format($imc, 2, ",") . "<span style=\"margin-left: 15px;\">" . ($imc > 25 ? "Seu peso está fora do IMC ideal" : "Você está saudável") . "</span>!</p>";

    echo "<p>O seu peso ideal é: " . number_format($pesoIdeal, 2, ",") . "Kg</p>";

?>