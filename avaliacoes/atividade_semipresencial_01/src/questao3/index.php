<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        .mb {
            margin-bottom: 10px;
        }
        .container {
            margin-left: auto;
            margin-right: auto;
            width: fit-content;
            text-align: center;
        }
    </style>
    <title>Questão 3 - ASP 01</title>
</head>
<body>
    <div class="mb container">
        <label for="estrelas">Número de estrelas para gerar</label>
        <select class="mb" name="estrelas" id="estrelas">
            <?php
                for ($i = 0; $i < 10; $i++)
                {
                    echo "<option value=\"" . ($i + 1) . "\">" . ($i + 1) . "</option>";
                }
            ?>
        </select>
        <br/>
        <input type="submit" class="btn btn-primary" onclick="enviarForm()" value="Gerar">
    </div>

    <div id="response" class="container"></div>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script>
        function enviarForm()
        {
            $("#response").html("<p>Carregando...</p>");
            $.ajax({
                url: "./processamento.php",
                type: "POST",
                data: {
                    num: $("#estrelas").find(":selected").val()
                },
                success: (res) => {
                    $("#response").html(res);
                },
                error: (res) => {
                    $("#response").html(res);
                }
            });
        }
    </script>
</body>
</html>