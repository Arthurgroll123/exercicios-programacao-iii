<?php
    if (!isset($_POST["num"]) || $_POST["num"] < 1 || $_POST["num"] > 10)
    {
        echo "Número de estrelas inválido!";
    }

    $img = "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b4/The_Sun_by_the_Atmospheric_Imaging_Assembly_of_NASA%27s_Solar_Dynamics_Observatory_-_20100819.jpg/300px-The_Sun_by_the_Atmospheric_Imaging_Assembly_of_NASA%27s_Solar_Dynamics_Observatory_-_20100819.jpg";

    for ($i = 0; $i < $_POST["num"]; $i++)
    {
        echo "<img style=\"width: 50px; margin-right: 5px;\" src=\"$img\"/>";
    }
?>