<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        .mb {
            margin-bottom: 10px;
        }
        .container {
            margin-left: auto;
            margin-right: auto;
            width: fit-content;
            text-align: center;
        }

        .h1 {
            margin-left: 0px;
            margin-right: 0px;
            width: fit-content;
        }
    </style>
    <title>Questão 2 - ASP 01</title>
</head>
<body>
    <div class="container">
        <h1>Registro de academia</h1>
    </div>
    <div class="mb container">
        <label for="nome">Nome</label>
        <input type="text" name="nome" id="nome">
        <label for="dataNasc">Data de Nascimento</label>
        <input type="date" name="dataNasc" id="dataNasc">
        <label for="genero">Gênero</label>
        <select class="mb" name="genero" id="genero">
            <option value="Masculino">Masculino</option>
            <option value="Feminino">Feminino</option>
        </select>
        <br/>
        <label for="estado">Estado</label>
        <input type="text" name="estado" id="estado"/>
        <label for="cidade">Cidade</label>
        <input class="mb" type="text" name="cidade" id="cidade"/>
        <br/>
        <label for="rua">Rua</label>
        <input type="text" name="rua" id="rua"/>
        <label for="complemento">Complemento</label>
        <input class="mb" type="text" name="complemento" id="complemento"/>
        <br/>
        <label>Atividades</label>
        <br/>
        <label for="atv1">Academia</label>
        <input type="checkbox" name="atividade[]" id="academia"/>
        <br/>
        <label for="atv1">Dança</label>
        <input type="checkbox" name="atividade[]" id="danca"/>
        <br/>
        <label for="atv1">Natação</label>
        <input type="checkbox" name="atividade[]" id="natacao"/>
        <br/>
        <label for="atv1">Hidroginástica</label>
        <input type="checkbox" name="atividade[]" id="hidroginastica"/>
        <br/>
        <label for="atv1">Judô</label>
        <input type="checkbox" name="atividade[]" id="judo"/>
        <br/>
        <button class="btn btn-primary" onclick="enviarForm()">Enviar</button>
    </div>

    <div id="response" class="container"></div>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script>
        function getArrayValues(inJson)
        {
            let arrValues = [];
            let elems = $("input[name='atividade[]']:checked");

            Array.from(elems).forEach((elem) => {
                arrValues.push(elem.id);
            });

            return inJson ? JSON.stringify(arrValues) : arrValues;
        }

        function getEndereco()
        {
            return {
                estado: $("#estado").val(),
                cidade: $("#cidade").val(),
                rua: $("#rua").val(),
                complemento: $("#complemento").val(),
                genero: $("#genero").find(":selected").val()
            }
        }

        function enviarForm()
        {
            $("#response").html("<p>Carregando...</p>");
            $.ajax({
                url: "./processamento.php",
                type: "POST",
                data: {
                    nome: $("#nome").val(),
                    dataNasc: $("#dataNasc").val(),
                    endereco: getEndereco(),
                    atividades: getArrayValues(true)
                },
                success: (res) => {
                    $("#response").html(res);
                },
                error: (res) => {
                    $("#response").html(res);
                }
            });
        }
    </script>
</body>
</html>