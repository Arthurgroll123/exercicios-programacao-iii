<?php
    $atividades = json_decode($_POST["atividades"]);
    $idade = (new DateTime($_POST["dataNasc"]))->diff(new DateTime("NOW"))->y;

    echo "<p>Idade:" . $idade . "<br/>Atividades matriculadas:</p><ul>";

    foreach($atividades as $atividade)
    {
        echo $atividade == 'academia' && $idade < 16 ? "" : "<li>$atividade</li>";
    }

    echo "</ul>";

    if (in_array("academia", $atividades) && $idade < 16)
        echo "<p><b>OBS: Matrícula da academia não pôde ser confirmada. Motivo: idade inválida!</p>";
?>