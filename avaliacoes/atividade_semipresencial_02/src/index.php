<?php
    require_once "./functions/functions.php";

    if (isset($_COOKIE["logged"]))
        header("Location: ./home.php");
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login | ASP 02</title>
    <?php require_once "./shared/scripts.php"; ?>
    <style>
        #msg-container {
            display: none;
            color: red;
        }
    </style>
</head>
<body>
    <h1>Página de Login</h1>
    <div>
        <input type="text" placeholder="Usuário" id="user" />
        <input type="password" placeholder="Senha" id="pass" />

        <input type="button" value="Entrar" onclick="logar(window.document.querySelector('#user').value, window.document.querySelector('#pass').value)" />
    </div>
    <div id="msg-container">
        <p id="msg"></p>
    </div>

    <script>
        function logar(usuario, pass)
        {
            if (usuario.length == 0 || pass.length == 0)
            {
                window.document.querySelector("#msg-container").style.display = "inline-block";
                window.document.querySelector("#msg").innerHTML = "Preencha todos os campos para prosseguir!";
            }

            $.ajax({
                url: './functions/functions.php',
                type: 'POST',
                data: {
                    function: "logar",
                    username: usuario,
                    password: pass,
                },
                success: (res) => {
                    if (res.includes("/"))
                        window.location.href = res;

                    window.document.querySelector("#msg-container").style.display = "inline-block";
                    window.document.querySelector("#msg").innerHTML = res;
                },
                error: (res) => {

                },
            });
        }
    </script>
</body>
</html>