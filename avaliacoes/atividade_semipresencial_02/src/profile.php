<?php
    require_once "./functions/functions.php";

    if (!isset($_COOKIE["logged"]))
        header("Location: ./index.php");

    $userInfo = json_decode($_COOKIE["currentUser"], true);
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Perfil | ASP 02</title>
    <?php require_once "./shared/scripts.php"; ?>
</head>
<body>
    <a href="./home.php" id="voltar">Voltar à página Home</a>
    <p>Imagem de perfil: </p>
    <img id="user-image" style="max-width: 256px;" src="data:image/png;base64,<?php echo file_get_contents($userInfo["profile_picture"]); ?>" />
    <div>
        <p>Trocar imagem de perfil</p>
        <input id="imagem-upload" onchange="loadImage()" type="file" max-size="10485760" accept=".png" value="Nova imagem" />
        <input type="button" onclick="changeImage()" value="Trocar imagem" />
    </div>
    <div>
        <p><a href="javascript:void(0);" id="sair" onclick="logout()">Fazer logout</a></p>
    </div>
    <script>
        let image;

        function getBase64(file) {
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function () {
                image = reader.result;
            };
            reader.onerror = function (error) {
                console.log('Error: ', error);
            };
        }
        
        function loadImage()
        {
            getBase64(window.document.querySelector("#imagem-upload").files[0]);
        }

        function changeImage()
        {
            $.ajax({
                url: './functions/functions.php',
                type: 'POST',
                data: {
                    function: "trocarImagem",
                    newImage: image.substring(image.indexOf(",") + 1),
                },
                success: (res) => {
                    window.document.querySelector("#user-image").setAttribute("src", "data:image/png;base64," + res);
                },
                error: (res) => {

                },
            });
        }

        function logout()
        {
            $.ajax({
                url: './functions/functions.php',
                type: 'POST',
                data: {
                    function: "logout",
                },
                success: (res) => {
                    window.location.href = "./index.php";
                },
                error: (res) => {

                },
            });
        }
    </script>
</body>
</html>