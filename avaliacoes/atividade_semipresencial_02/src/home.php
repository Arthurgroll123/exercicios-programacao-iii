<?php
    require_once "./functions/functions.php";

    if (!isset($_COOKIE["logged"]))
        header("Location: ./index.php");

    $userInfo = json_decode($_COOKIE["currentUser"], true);
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home | ASP 02</title>
    <?php require_once "./shared/scripts.php"; ?>
</head>
<body>
    <h1>Boas vindas <?php echo $userInfo["name"]; ?>!</h1>
    <div id="container">
        <a href="./profile.php" id="check-profile">Perfil</a>
    </div>
</body>
</html>