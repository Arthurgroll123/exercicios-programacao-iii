<?php

    // Possíveis logins
    $GLOBALS["possibleLogins"] = [
        "q1" => [
            "username" => "q1",
            "name" => "Q. 1",
            "password" => hash("sha256", "q1"),
            "profile_picture" => "./profile_pictures/default.txt",
        ],
        "w2" => [
            "username" => "w2",
            "name" => "W. 2",
            "password" => hash("sha256", "w2"),
            "profile_picture" => "./profile_pictures/default.txt",
        ],
        "e3" => [
            "username" => "e3",
            "name" => "E. 3",
            "password" => hash("sha256", "e3"),
            "profile_picture" => "./profile_pictures/default.txt",
        ],
    ];

    /**
     * Função que verifica se as credenciais do usuário são válidas
     * 
     * @param user      string contendo o username do usuário
     * @param passHash  string contendo a hash (SHA256) da senha do usuário
     * 
     * @return bool     um valor booleano
     */
    function login($user, $pass)
    {
        if (isset($GLOBALS["possibleLogins"][$user]) && $GLOBALS["possibleLogins"][$user]["password"] == $pass)
        {
            setUserLoggedIn($GLOBALS["possibleLogins"][$user]);
            return true;
        }

        return false;
    }

    /**
     * Função que seta o cookie com as informações do usuário logado para obter posteriormente
     * 
     * @param username  o username do usuário
     * @param userInfo  array contendo informações do usuário (mesmos campos da variável global possibleLogins)
     */
    function setUserLoggedIn($userInfo)
    {
        setcookie("logged", "true", time() + 3600 * 24, '/');

        if (!isset($_COOKIE["profile_pictures"]) || !isset(json_decode($_COOKIE["profile_pictures"], true)[$userInfo["username"]]))
        {
            setcookie("currentUser", json_encode($userInfo), time() + 3600 * 24, '/');
        }
        else
        {
            $userInfo["profile_picture"] = json_decode($_COOKIE["profile_pictures"], true)[$userInfo["username"]];
            setcookie("currentUser", json_encode($userInfo), time() + 3600 * 24, '/');
        }
    }

    /**
     * Retorna um caminho com um nome de arquivo ainda não utilizado para guardar o arquivo de upload
     * 
     * @return string   uma string contendo o caminho de um arquivo liberado (ainda não existente) para gravar informações
     */
    function getUnusedFilePath()
    {
        $nome = "default.txt";

        while (file_exists("../profile_pictures/" . $nome))
            $nome = hash('sha256', $nome . rand(1, 9999)) . ".txt";

        return "./profile_pictures/" . $nome;
    }

    /**
     * Função que muda a foto de perfil do usuário
     * 
     * @param userInfo  array contendo informações do usuário (mesmos campos da variável global possibleLogins)
     * 
     * @return string   a imagem que foi trocada (em base64)
     */
    function changeUserProfilePicture($newImage)
    {
        $picturePath = getUnusedFilePath();

        $newUserInfo = json_decode($_COOKIE["currentUser"], true);

        $newUserInfo["profile_picture"] = $picturePath;

        if (isset($_COOKIE["profile_pictures"]))
        {
            $pictures = json_decode($_COOKIE["profile_pictures"], true);
            $pictures[$newUserInfo["username"]] = $newUserInfo["profile_picture"];

            setcookie("profile_pictures", json_encode($pictures), time() + 3600 * 24, '/');
        }
        else
        {
            $pictures = [];
            $pictures[$newUserInfo["username"]] = $newUserInfo["profile_picture"];

            setcookie("profile_pictures", json_encode($pictures), time() + 3600 * 24, '/');
        }

        setcookie("currentUser", json_encode($newUserInfo), time() + 3600 * 24, '/');
        
        file_put_contents("." . $picturePath, $newImage);

        return file_get_contents("." . $picturePath);
    }

    /**
     * Função que desloga o usuário atual corretamente
     */
    function logout()
    {
        //setcookie("currentUser", "", time() - 3600, '/');
        //unset($_COOKIE["currentUser"]);

        setcookie("logged", "", time() - 3600, '/');
        unset($_COOKIE["logged"]);
    }

    if (isset($_POST["function"]))
    {
        $function = $_POST["function"];

        if (strtolower($function) == "logar")
        {
            if (!login($_POST["username"], hash('sha256', $_POST["password"])))
                echo "Credenciais inválidas!";
            else
                echo "./home.php";
        }

        if (strtolower($function) == "logout")
        {
            logout();
        }

        if (strtolower($function) == "trocarimagem")
        {
            echo changeUserProfilePicture($_POST["newImage"]);
        }
    }
?>