<!--
    Q: Você precisa de mais uma variável para fazer a subtração de A por B, utiliando o código do Exercício 1?

    A: Não, pois como preciso apenas mostrar o resultado e não há necessidade de utilizar o resultado novamente, posso apenas
    fazer a operação no echo (output), sem a necessidade de guardar o resultado obtido em uma variável.
-->

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="./shared/styles.css"/>
    <title>Exercício 2 - Aula 01</title>
</head>
    <body>
        <h1>Exercício 2</h1>
        <div>
            <a href="./index.html">Voltar ao menu</a>
            <p>
                <?php
                    $a = 3;
                    $b = 5;
                    $resultado = $a + $b;

                    echo "O valor da variável A é " . $a . "</br>";
                    echo "O valor da variável B é " . $b . "</br>";
                    echo "A soma de A e B é " . $resultado . ".<br/>";
                    echo "A subtração de A por B é " . ($a - $b) . ".";
                ?>
            </p>
        </div>
    </body>
</html>