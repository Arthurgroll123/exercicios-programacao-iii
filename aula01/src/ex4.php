<!--
    Q1: Ter as variáveis de nome c e C gera algum conflito para o programa? Por quê?

    A1: Não, porque o PHP é case-sensitive, e as variáveis c e C são tratadas da mesma maneira que as variáveis a e b, por exemplo.

    Q2: Em sua opinião, nomear as variáveis como c e C parece uma boa prática?

    A2: Não, porque nomes de variáveis deve ser intuitivos e dar alguma noção básica dos propósitos dessas variáveis no programa.

    Q3: Que outros nomes poderiam ser dados para a variável C? Há alguma restrição a nomes de variáveis em PhP?

    A3: Poderia ter sido utilizado o nome valor4. Sim, no PHP não é possível utilizar símbolos (salvas exceções do underline/underscore "_" e
    do cifrão "$") e não é possível iniciar o nome da variável com um número. No caso particular do cifrão, se o cifrão for utilizado ele deve
    ser utilizado como sendo o primeiro caracter do nome da variável, e após o cifrão deve vir o nome de uma outra variável pré-existente. Isso vai
    fazer com que o nome final da variável criada seja o valor contido nessa variável pré-existente a qual se utilizou o nome. Um exemplo:

    $valor3 = "valor1";
    $valor1 = "var1337";
    $valor2 = $valor3;
    $var1337 = 1337;

    echo $$$valor2;

    O exemplo pode ser dividido em três partes:

    Na primeira, o "nome" da variável será transformado para: $$valor1, porque o valor contido na variável $valor2 é "valor1".

    Na segunda, o "nome" da variável agora será transformado para $var1337, porque o valor contido na variável $valor1 é "var1337".

    Na terceira e última parte, mostramos o valor de $var1337, que é 1337.
-->

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="./shared/styles.css"/>
    <title>Exercício 4 - Aula 01</title>
</head>
    <body>
        <h1>Exercício 4</h1>
        <div>
            <a href="./index.html">Voltar ao menu</a>
            <p>
                <?php
                    $a = 3;
                    $b = 5;
                    $resultado = $a + $b;

                    echo "O valor da variável A é " . $a . "</br>";
                    echo "O valor da variável B é " . $b . "</br>";
                    echo "A soma de A e B é " . $resultado . ".<br/>";
                    echo "A subtração de A por B é " . ($a - $b) . ".<br/>";

                    $c = 10;

                    echo "O resultado do produto de a, b e c é: " . ($a * $b * $c) . "<br/>";

                    $C = 1337; // valor escolhido por mim, já que não há valor especificado no exercício

                    echo "O resultado da soma de a + b + c + C é: " . ($a + $b + $c + $C);
                ?>
            </p>
        </div>
    </body>
</html>