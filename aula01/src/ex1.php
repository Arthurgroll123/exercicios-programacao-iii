<!--
    Q: Com o programa pronto, para visualizar no navegador o resultado do
    programa em funcionamento, de que maneira devemos abrir o arquivo?

    A: Devemos ter um servidor rodando (como o que eu faço, por exemplo, em que rodo um container
    Docker que roda uma imagem com Apache, e que expõe para mim a porta 3301 mapeada para a porta 80,
    que utilizo para acessar as páginas pelo navegador) e colocar o arquivo PHP no diretório sendo
    servido, e acessá-lo através do servidor. Um exemplo é o XAMPP, que possui por padrão os arquivos disponíveis
    para acesso na pasta "C:\xampp\htdocs". Se tivéssemos um arquivo "index.php", deveríamos colocá-lo dentro de
    "C:\xampp\htdocs", e acessá-lo no navegador utilizando o "localhost".
-->

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="./shared/styles.css"/>
    <title>Exercício 1 - Aula 01</title>
</head>
    <body>
        <h1>Exercício 1</h1>
        <div>
            <a href="./index.html">Voltar ao menu</a>
            <p>
                <?php
                    $a = 3;
                    $b = 5;
                    $resultado = $a + $b;

                    echo "O valor da variável A é " . $a . "</br>";
                    echo "O valor da variável B é " . $b . "</br>";
                    echo "A soma de A e B é " . $resultado . ".";
                ?>
            </p>
        </div>
    </body>
</html>